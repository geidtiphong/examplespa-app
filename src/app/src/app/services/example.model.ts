export interface ExampleData {
  Id: number;
  Name: string;
  IsActive: string;
  CreatedBy: string;
  CreatedDate: Date;
  UpdatedBy: string;
  UpdatedDate: Date;
}

export interface ResponseClient<T> {
  DateTime: Date;
  ErrorsMessage: string[];
  Entities: T;
}
