import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ExampleData, ResponseClient } from './example.model';

@Injectable({
  providedIn: 'root',
})
export class ExampleService {
  private apiUrl: string;
  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.apiUrl}/ExampleCRUD`;
  }

  getAll() {
    return this.http.get<ResponseClient<ExampleData[]>>(`${this.apiUrl}`);
  }

  getById(id: number) {
    return this.http.get<ResponseClient<ExampleData>>(`${this.apiUrl}/${id}`);
  }

  insert(criteria: ExampleData) {
    return this.http.post<ResponseClient<ExampleData>>(
      `${this.apiUrl}`,
      criteria
    );
  }

  update(criteria: ExampleData) {
    return this.http.put<ResponseClient<ExampleData>>(
      `${this.apiUrl}`,
      criteria
    );
  }

  delete(id: number) {
    return this.http.delete<ResponseClient<ExampleData>>(
      `${this.apiUrl}/${id}`
    );
  }
}
