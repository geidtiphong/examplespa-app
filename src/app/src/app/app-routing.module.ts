import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExampleTableComponent } from './pages/example-table/example-table.component';
import { ExampleFormComponent } from './pages/example-form/example-form.component';

const routes: Routes = [
  { path: 'example-table', component: ExampleTableComponent },
  { path: 'example-form', component: ExampleFormComponent },
  { path: '', pathMatch: 'full', redirectTo: '/example-table' },
  {
    path: 'welcome',
    loadChildren: () =>
      import('./pages/welcome/welcome.module').then((m) => m.WelcomeModule),
  },
  { path: '**', redirectTo: 'example-table' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
