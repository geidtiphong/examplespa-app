import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ExampleService } from 'src/app/services/example.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExampleData } from 'src/app/services/example.model';
import { RouteParams } from 'src/app/services/route.paramters.model';

@Component({
  selector: 'app-example-form',
  templateUrl: './example-form.component.html',
  styleUrls: ['./example-form.component.scss'],
})
export class ExampleFormComponent implements OnInit {
  validateForm!: FormGroup;
  isVisible: boolean;
  exampleData: ExampleData;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private exampleService: ExampleService
  ) {}

  ngOnInit(): void {
    this.isVisible = false;
    this.validateForm = this.fb.group({
      Id: [0],
      Name: [null, [Validators.required]],
    });

    this.exampleData = Object.assign({}, this.validateForm.getRawValue());
    this.route.queryParams.subscribe((params: RouteParams) => {
      this.exampleData.Id = +params.id || 0;
      if (this.exampleData.Id === 0) {
        return false;
      }

      this.initialzeDataToForm();
    });
  }

  get form() {
    return this.validateForm.controls;
  }

  initialzeDataToForm() {
    this.exampleService
      .getById(this.exampleData.Id)
      .subscribe((response) => this.validateForm.patchValue(response.Entities));
  }

  insert() {
    this.exampleService
      .insert(this.exampleData)
      .subscribe(() => this.router.navigate(['/example-table']));
  }

  update() {
    this.exampleService
      .update(this.exampleData)
      .subscribe(() => this.router.navigate(['/example-table']));
  }

  submitForm() {
    // tslint:disable-next-line: forin
    for (const i in this.form) {
      this.form[i].markAsDirty();
      this.form[i].updateValueAndValidity();
    }

    if (this.validateForm.invalid) {
      return false;
    }

    this.isVisible = true;
  }

  handleOk() {
    this.isVisible = false;
    this.exampleData = Object.assign({}, this.validateForm.getRawValue());

    if (this.exampleData.Id === 0) {
      this.insert();
    } else {
      this.update();
    }
  }
}
