import { Component, OnInit } from '@angular/core';
import { ExampleData } from 'src/app/services/example.model';
import { ExampleService } from 'src/app/services/example.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-example-table',
  templateUrl: './example-table.component.html',
  styleUrls: ['./example-table.component.scss'],
})
export class ExampleTableComponent implements OnInit {
  dataSet: ExampleData[];
  constructor(private router: Router, private exampleService: ExampleService) {}

  ngOnInit(): void {
    this.dataSet = [];
    this.getAll();
  }

  getAll() {
    this.exampleService
      .getAll()
      .subscribe((response) => (this.dataSet = response.Entities));
  }

  goToForm() {
    this.router.navigate(['/example-form']);
  }

  goToFormWithId(id: number) {
    this.router.navigate(['/example-form'], { queryParams: { id } });
  }

  confirm(id: number) {
    this.exampleService.delete(id).subscribe(() => this.getAll());
  }
}
