namespace WebApiDotNetCore.Utilities {
  public class ApplicationDatabase {
    public string PrimaryDatabaseConnectionString { get; set; }
  }
}