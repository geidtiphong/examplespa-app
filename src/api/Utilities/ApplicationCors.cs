using System;
using System.Collections.Generic;

namespace WebApiDotNetCore.Utilities {
  public class ApplicationCors {
    public string[] AllowedHosts { get; set; }
    public string[] AllowedMethods { get; set; }
  }
}