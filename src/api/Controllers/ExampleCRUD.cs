﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApiDotNetCore.RequestDTO;
using WebApiDotNetCore.ResponseDTO;
using WebApiDotNetCore.Utilities;

namespace WebApiDotNetCore.Controllers {
  [ApiController]
  [Route ("api/[controller]")]
  public class ExampleCRUDController : ControllerBase {
    private readonly SqlConnection _connection;
    public ExampleCRUDController (IOptions<ApplicationDatabase> applicationDatabase) {
      _connection = new SqlConnection (applicationDatabase.Value.PrimaryDatabaseConnectionString);
    }

    [HttpGet]
    public async Task<IActionResult> Search () {
      var results = await _connection.GetAllAsync<ExampleCRUDResponse> ();
      return Ok (results);
    }

    [HttpGet ("{id:int}")]
    public async Task<IActionResult> Find (int id) {
      var results = await _connection.GetAsync<ExampleCRUDResponse> (id);
      return Ok (results);
    }

    [HttpPost]
    public async Task<IActionResult> Insert ([FromBody] ExampleCRUDRequest criteria) {
      await _connection.InsertAsync (criteria);
      return Created ("", criteria);
    }

    [HttpPut]
    public async Task<IActionResult> Update ([FromBody] ExampleCRUDRequest criteria) {
      await _connection.UpdateAsync (criteria);
      return Ok (criteria);
    }

    [HttpDelete ("{id:int}")]
    public async Task<IActionResult> Delete (int id) {
      var criteria = new ExampleCRUDRequest () { Id = id };
      var results = await _connection.DeleteAsync (criteria);
      return NoContent ();
    }
  }
}