using System;

namespace WebApiDotNetCore.RequestDTO {
  public class BaseRequest {
    public string IsActive { get; set; } = "Y";
    public string CreatedBy { get; set; } = "Pobx";
    public DateTime CreatedDate { get; set; } = DateTime.Now;
    public string UpdatedBy { get; set; } = "Pobx";
    public DateTime UpdatedDate { get; set; } = DateTime.Now;
  }
}