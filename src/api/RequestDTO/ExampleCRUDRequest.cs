using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;
using KeyAttribute = Dapper.Contrib.Extensions.KeyAttribute;

namespace WebApiDotNetCore.RequestDTO {
  [Table ("ExampleDemo")]
  public class ExampleCRUDRequest : BaseRequest {
    [Key]
    public int Id { get; set; } = 0;
    [Required(ErrorMessage="อย่าลืมใส่เด้อพี่น้อง !!!")]
    public string Name { get; set; }
  }
}