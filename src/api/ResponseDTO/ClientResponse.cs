using System;
using System.Collections.Generic;

namespace WebApiDotNetCore.ResponseDTO {
  public class ClientResponse<T> {
    public DateTime CurrentDateTime { get; } = DateTime.UtcNow;
    public List<string> ErrorsMessage { get; set; } = new List<string> ();
    public T Entities { get; set; }
  }
}