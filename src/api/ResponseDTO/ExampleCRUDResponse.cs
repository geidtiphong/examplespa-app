using Dapper.Contrib.Extensions;

namespace WebApiDotNetCore.ResponseDTO {
  [Table ("ExampleDemo")]
  public class ExampleCRUDResponse : BaseResponse {
    [Key]
    public int Id { get; set; }
    public string Name { get; set; }
  }
}