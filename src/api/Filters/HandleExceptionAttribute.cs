using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApiDotNetCore.ResponseDTO;

namespace WebApiDotNetCore.Filters {
  public class HandleExceptionAttribute : ExceptionFilterAttribute {

    public override void OnException (ExceptionContext context) {
      var response = new ClientResponse<string> ();

      response.ErrorsMessage.Add (context.Exception.Message);
      context.Result = new ObjectResult (response) {
        StatusCode = 500
      };

      context.ExceptionHandled = true;

    }
  }
}