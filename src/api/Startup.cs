using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using WebApiDotNetCore.Filters;
using WebApiDotNetCore.Utilities;

namespace WebApiDotNetCore {
  public class Startup {
    public Startup (IConfiguration configuration) {
      Configuration = configuration;
    }
    const string SiteCorsPolicy = "SiteCorsPolicy";
    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices (IServiceCollection services) {
      services.Configure<ApplicationDatabase> (Configuration.GetSection ("ConnectionStrings"));
      //  var AllowedCors = services.Configure<ApplicationCors> (Configuration.GetSection ("ApplicationCors"));
      var AllowedCors = new ApplicationCors ();
      Configuration.GetSection ("ApplicationCors").Bind (AllowedCors);
      services.AddCors (options => {
        options.AddPolicy (SiteCorsPolicy, builder => {
          // builder.AllowAnyOrigin ().AllowAnyMethod ().AllowAnyHeader ().AllowCredentials ().WithExposedHeaders ("Content-Disposition");
          builder.WithOrigins (AllowedCors.AllowedHosts).WithMethods (AllowedCors.AllowedMethods).AllowAnyHeader ();
        });
      });

      services.AddControllers (options => {
          options.Filters.Add (typeof (HandleExceptionAttribute));
          options.Filters.Add (typeof (ResponseModelAttribute));
        })
        .AddNewtonsoftJson (options => {
          options.SerializerSettings.ContractResolver = new DefaultContractResolver ();
        });

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
      }

      app.UseCors (SiteCorsPolicy);

      // app.UseHttpsRedirection ();

      app.UseRouting ();

      app.UseAuthorization ();

      app.UseEndpoints (endpoints => {
        endpoints.MapControllers ();
      });
    }
  }
}